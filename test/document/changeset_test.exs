defmodule DurangoDocumentChangesetTest do
  use ExUnit.Case
  require Durango
  alias DurangoExample.{Repo, Person}
  doctest Durango.Document.Changeset

  setup do
    DurangoExample.Repo.start_link([])
    :ok
  end

  test "will create a changeset of a null struct" do
    assert %Person{} |> Person.changeset(%{name: "namerino"}) == %Durango.Document.Changeset{
             changes: %{name: "namerino"},
             document: %DurangoExample.Person{
               _id: nil,
               _key: nil,
               _rev: nil,
               age: nil,
               name: nil
             },
             errors: [],
             module: DurangoExample.Person,
             params: %{name: "namerino"}
           }
  end

  test "will update an already predefined value" do
    assert %Person{name: "lel"} |> Person.changeset(%{name: "namerino"}) ==
             %Durango.Document.Changeset{
               changes: %{name: "namerino"},
               document: %DurangoExample.Person{
                 _id: nil,
                 _key: nil,
                 _rev: nil,
                 age: nil,
                 name: "lel"
               },
               errors: [],
               module: DurangoExample.Person,
               params: %{name: "namerino"}
             }
  end

  test "will insert an changeset" do
    person =
      %Person{}
      |> Person.changeset(%{name: "lel"})

    person_query = person |> Durango.Repo.convert_changeset() |> Durango.Repo.insert_query()

    assert to_string(person_query) == "INSERT @doc INTO @@doc_collection RETURN NEW"
    response = person |> Repo.insert!

    # Check if response matches
    with %{_id: id, name: name, age: age} <- response do
      assert id != nil
      assert age == nil
      assert name == "lel"
    else
      _ -> assert false
    end
  end

  test "will update with an changeset" do
    person1 = %Person{name: "bleep", age: 2} |> Repo.insert!
    person2 =
    person1
    |> Person.changeset(%{name: "lel"})

    person_query = person2 |> Durango.Repo.convert_changeset() |> Durango.Repo.update_query()
    assert to_string(person_query) == "LET this_doc = DOCUMENT(@doc__id) UPDATE this_doc WITH @doc IN @@doc_collection RETURN NEW"
    response = person2 |> Repo.update!

    # Check if response matches
    with %{_id: id, name: name, age: age, _rev: rev} <- response do
      assert id != nil
      assert age == 2
      assert name == "lel"
      assert person1._rev != rev
    else
      _ -> assert false
    end
  end
end
