defmodule DurangoDocumentEdgeTest do
    use ExUnit.Case
    doctest Durango.Document
    require Durango
    alias DurangoExample.{Person,HasFriend,Graph, Repo}
    setup do
        DurangoExample.Repo.start_link([])
        :ok
    end

    test "non edge collection does not have _to and _from but edge collection do" do
        assert Person.__document__(:fields) == [age: %{type: :integer}, name: %{type: :string}, _rev: nil, _key: nil, _id: nil]
        assert HasFriend.__document__(:fields) == [best_friend: %{type: :boolean, default: false}, _from: nil, _to: nil, _rev: nil, _key: nil, _id: nil]
    end

    test "add two persons and add a has_friend connection between them" do
        assert %Person{} = person1 = %Person{
            name: "Jason",
            age: 30,
        } |> Repo.insert!

        assert %Person{} = person2 = %Person{
            name: "Jasmine",
            age: 32,
        } |> Repo.insert!

        response = Graph.as_struct(person1, HasFriend, person2)
        |> Repo.insert!

        with %DurangoExample.HasFriend{_id: id, _to: to, _from: from, best_friend: best_friend} <- response do
            assert from == person1._id
            assert id != nil
            assert to == person2._id
            assert best_friend == false
        else
            _ -> assert false
        end
    end

    test "add two persons and add a has_friend connection between them and data added" do
        assert %Person{} = person1 = %Person{
            name: "Jason",
            age: 30,
        } |> Repo.insert!

        assert %Person{} = person2 = %Person{
            name: "Jasmine",
            age: 32,
        } |> Repo.insert!

        response = Graph.as_struct(person1, HasFriend, person2, %{best_friend: true})
        |> Repo.insert!

        with %DurangoExample.HasFriend{_id: id, _to: to, _from: from, best_friend: best_friend} <- response do
            assert from == person1._id
            assert id != nil
            assert best_friend == true
            assert to == person2._id
        else
            _ -> assert false
        end
    end
end
