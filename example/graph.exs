defmodule DurangoExample.Graph do
    use Durango.Graph
    alias DurangoExample.{Person,HasFriend}
  
    graph :example_graph do
      relationship Person, HasFriend, Person
    end
end  