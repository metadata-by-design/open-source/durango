defmodule DurangoExample.Person do
  use Durango.Document
  import Durango.Document.Changeset

  document :persons do
    field :name, :string
    field :age,  :integer
  end

  def changeset(person, params \\ %{} ) do
    person
    |> cast(params, [:name, :age])
  end
end
